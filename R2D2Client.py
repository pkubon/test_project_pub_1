import time
import chromedriver_autoinstaller

from os import environ
from os.path import join
from tkinter import messagebox
from asyncio.log import logger
from selenium import webdriver
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as exp_cond
from selenium.webdriver.support.ui import WebDriverWait


class R2D2Client:
    def __init__(self):
        self.quick_timeout = 5
        self.timeout = 30

        self._chrome_userdata = join(dict(environ)["LOCALAPPDATA"], "Google\\Chrome\\User data\\Default")

        self.BASE_URL = "https://r2d2-peopleanalytics.capgemini.com/people/"
        self.driver = None
        self.__options = webdriver.ChromeOptions()
        self.__options.add_experimental_option("detach", True)  # keep browser open after running script
        self.__options.add_experimental_option("useAutomationExtension", False)
        self.__options.add_experimental_option("excludeSwitches", ["enable-automation", 'enable-logging'])
        self.__options.add_argument("--no-sandbox")
        self.__options.add_argument("--headless")
        self.__options.headless = False  # hide or show browser window
        self.__options.add_argument(f"user-data-dir={self._chrome_userdata}")

#        prefs = {"download.default_directory":  r"C:New_Download"}
#        self.__options.add_experimental_option("prefs", prefs)
#        print(self.__options.experimental_options)



    def start_chrome_borwser(self):
        """Run chrome web driver"""
        chromedriver_autoinstaller.install()
        self.driver = webdriver.Chrome(options=self.__options)

    def go_to_login_page(self):
        print("ok")

# ----------------------------------------------------------------------------------------

    def does_alert_pops_out(self):
        """Return true if alert pops out, if not, return false"""
        try:
            WebDriverWait(self.driver, self.quick_timeout).until(
                exp_cond.alert_is_present()
            )
            return True
        except (ElementNotVisibleException, TimeoutException):
            return False

    def close_browser(self):
        """Close browser if driver is not None"""
        if self.driver is not None:
            self.driver.close()

    def check_page_is_loaded(self):
        """Check that r2d2 page is working by checking that main.js script is already loaded"""
        element_xpath = "//script[@src='main.js']"
        self.driver.get(self.BASE_URL)
        WebDriverWait(self.driver, self.timeout).until(
            exp_cond.presence_of_element_located((By.XPATH, element_xpath))
        )

# ----------------------------------------------------------------------------------------


    def open_employees_page(self, user_url: str):
        """Open PeopleAnalitics v5 page and search for user passed by ggid"""
        try:
            self.driver.get(user_url)
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to Open employee's page", exception_msg=e)

    def get_profile_page_link(self) -> str:
        """Returns url to user page or run raising exception procedure"""
        try:
            xpath_selector = "//*[@id='offcanvasLeft']/div[3]/div/div/div[12]/div[2]/a"
            return WebDriverWait(self.driver, self.timeout).until(
                exp_cond.presence_of_element_located((By.XPATH, xpath_selector))
            ).get_attribute("href")
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to get profile page link", exception_msg=e)

    def open_profile_page(self, profile_page_link: str):
        """Open user page profile"""
        try:
            self.driver.get(profile_page_link)
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to Open Profile page", exception_msg=e)

    def check_if_url_opens_correctly(self, profile_page_link: str):
        """Run raising exception procedure when taken url is different with actual one"""
        if not (self.driver.current_url == profile_page_link):
            msg = "Possibly wrong employee ID or the employee is not active"
            print("AppHelper.run_raising_exception_procedure(my_msg=msg, exception_msg=msg)")

    def wait_till_1st_iframe_loads(self):  # Explicitly wait for element to load: 1st iframe
        """Wait till iframe element loaded in page"""
        try:
            xpath_selector = "//iframe[@name='RtnCache']"
            WebDriverWait(self.driver, self.timeout).until(
                exp_cond.presence_of_element_located((By.XPATH, xpath_selector))
            )
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Iframe1 not loaded", exception_msg=e)

    def switch_to_iframe_with_edit_button(self):
        """Switch to 'Edit' iframe button"""
        try:
            xpath_selector = "//iframe[@id='rwp']"
            element = WebDriverWait(self.driver, self.timeout).until(
                exp_cond.presence_of_element_located((By.XPATH, xpath_selector))
            )
            self.driver.switch_to.frame(element)
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to switch to iframe1", exception_msg=e)

    def click_edit_button(self):
        """Click "Edit" button"""
        try:
            xpath_selector = "//a[@id='cmd-edit']"
            WebDriverWait(self.driver, self.timeout).until(
                exp_cond.presence_of_element_located((By.XPATH, xpath_selector))
            ).click()
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to click 'Edit' button", exception_msg=e)

    def get_inner_html_of_existing_projects(self) -> str:
        """Gets html block in existing projects div"""
        try:
            xpath_selector = "//div[@id='wp-PHI_RES_ID_PHI']"
            return self.driver.find_element(By.XPATH, xpath_selector).get_attribute("innerHTML")
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to grab inner html of existing projects", exception_msg=e)

    def click_internal_experience(self):
        """Click 'Internal Experience' in vertical menu on left-hand side"""
        try:
            xpath_selector = "//li/a[@href='#wp-PHI_RES_ID_PHI']"
            WebDriverWait(self.driver, self.timeout).until(
                exp_cond.presence_of_element_located((By.XPATH, xpath_selector))
            ).click()
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to click 'Internal Experience', initial", exception_msg=e)

    def click_import_from_retain_button(self):
        """Click '+Import from Retain' button"""
        try:
            selector = "//*[@id='wp-PHI_RES_ID_PHI']/react-component/section/a[2]"
            self._scroll_and_click((By.XPATH, selector))
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to click 'Import from Retain', initial", exception_msg=e)

    def get_inner_html_of_retain_table(self) -> str:
        """Get innerHTML of Retain table"""  # TODO Try relative here
        try:
            xpath_selector = "/html/body/div[7]/div[2]/span/div[1]"
            return self.driver.find_element(By.XPATH, xpath_selector).get_attribute("innerHTML")
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to grab inner html of Retain table", exception_msg=e)

    def check_checkbox(self, selector: str):
        """Check checkbox in Retain Table"""
        try:
            self._scroll_and_click((By.XPATH, selector))
            #self._scroll_and_click((By.CSS_SELECTOR, selector))
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to check CSS selector", exception_msg=e)

    def click_ok_retain_table(self):
        """Click OK in Retain Table"""
        try:
            selector = "//*[@id='undefined-import-from-retain']/div[2]/input[1]"
            self._scroll_and_click((By.XPATH, selector))
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to click 'OK' Selector", exception_msg=e)

    def scroll_and_save_changes(self):
        """Scroll and save checked selector"""
        try:
            selector = "//div[@id='wp-PHI_RES_ID_PHI']/react-component/section/div/div[1]//input[@value='Save']"
            self._scroll_and_click((By.XPATH, selector))
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to save changes", exception_msg=e)

    def click_cancel_alert(self):
        """Click 'Cancel' after alert showing"""
        try:
            selector = "/html/body/div[7]/div[2]/span/div[2]/input[2]"  # TODO Try relative here
            self.driver.switch_to.alert.accept()
            self.driver.switch_to.default_content()
            self._scroll_and_click((By.XPATH, selector))
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to click Cancel, after Alert", exception_msg=e)

    def click_cancel_no_items_to_check(self):
        """Click 'Cancel' if there is no item to be selected"""
        try:
            selector = "/html/body/div[7]/div[2]/span/div[2]/input[2]"
            self._scroll_and_click((By.XPATH, selector))
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to click 'Cancel'", exception_msg=e)

    def click_exit_edit_mode_button(self):
        """ Click 'Exit edit mode' """
        try:
            selector = "//a[@id='cmd-save']"
            self._scroll_and_click((By.XPATH, selector))
        except Exception as e:
            print(e) # AppHelper.run_raising_exception_procedure(my_msg="Unable to click 'Exit edit mode'", exception_msg=e)

    def _scroll_and_click(self, selector: tuple[str, str]):
        """Scroll and click xpath selected element"""
        element = WebDriverWait(self.driver, self.timeout).until(
            exp_cond.presence_of_element_located(selector)
        )
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        time.sleep(2)
        element.click()
        time.sleep(1)


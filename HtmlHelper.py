from datetime import datetime
from bs4 import BeautifulSoup

from utils.HtmlTableModel import HtmlTableModel


class HtmlHelper:
    """Helper class to parse html from r2d2 web page"""

    @staticmethod
    def get_existing_projects(inner_html: str) -> list[str]:
        """Return list of existing projects."""
        soup = BeautifulSoup(inner_html, "lxml")
        return [h3.text for h3 in soup.find_all("h3")]

    @staticmethod
    def jobs_to_add(html_object: str, existing_projects: list[str], loc_format: str) -> list[HtmlTableModel]:
        """ Returns list of html table row to be checked with selenium. """
        table_model_list = HtmlHelper.__parse_html_table_to_table_model_list(html_object)
        return [
            model for model in table_model_list
            if not HtmlHelper.__end_in_future(model, loc_format) and
            HtmlHelper.__job_exist_in_existing_projects(model, existing_projects) and
            not HtmlHelper.__start_in_future(model, loc_format)
        ]

    @staticmethod
    def __parse_html_table_to_table_model_list(html_obj: str) -> list[HtmlTableModel]:
        """Converts html object to list of table row models."""
        soup = BeautifulSoup(html_obj, "lxml").table
        res = []
        tr_list = soup.tbody.find_all("tr")
        for i, tr in enumerate(tr_list):
            tab_row = tr.find_all("td")
            #selector = f"#undefined-import-from-retain > div.modal-dialog__body > table > tbody > " \
            #           f"tr:nth-child({i + 1}) > td:nth-child(1) > div > label"
            selector = f"//*[@id=\"undefined-import-from-retain\"]/div[1]/table/tbody/tr[{i+1}]/td[1]"
            res.append(HtmlTableModel(tab_row[1].text, tab_row[2].text, tab_row[3].text, tab_row[4].text, selector))

        return res

    @staticmethod
    def __end_in_future(model: HtmlTableModel, loc_format: str) -> bool:
        return datetime.strptime(model.end_date, loc_format) > datetime.now()

    @staticmethod
    def __start_in_future(model: HtmlTableModel, loc_format: str) -> bool:
        return datetime.strptime(model.start_date, loc_format) > datetime.now()

    @staticmethod
    def __job_exist_in_existing_projects(model: HtmlTableModel, existing_projects: list[str]) -> bool:
        return model.job_name in existing_projects



from dataclasses import dataclass


@dataclass()
class HtmlTableModel:
    job_name: str = ""
    client_name: str = ""
    start_date: str = ""
    end_date: str = ""
    selector: str = ""
    